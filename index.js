var inuse = false;
var inuseindex = 0;

function readTextFile(file, callback) {
    var rawFile = new XMLHttpRequest();
    rawFile.overrideMimeType("application/json");
    rawFile.open("GET", file, true);
    rawFile.onreadystatechange = function () {
        if (rawFile.readyState === 4 && rawFile.status == "200") {
            callback(rawFile.responseText);
        }
    }
    rawFile.send(null);
}

setInterval(() => {
    readTextFile("http://localhost/data.json", function (text) {
        var data = JSON.parse(text);
        console.log(data.data);
        if (inuse) {
            if (data.data[inuseindex] == 0) {
                inuse = false;
                if ("index.html" != window.location.href.replace(/^.*[\\\/]/, '')) location.href = "index.html";
            }
        } else {
            for (let index = 0; index < data.data.length; index++) {
                const element = data.data[index];
                if (element == 1) {
                    inuseindex = index;
                    inuse = true;
                    if ([index + 1] + ".html" != window.location.href.replace(/^.*[\\\/]/, '')) location.href = [index + 1] + ".html"
                }
            }
        }
    });
}, 1000);
