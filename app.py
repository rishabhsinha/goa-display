import RPi.GPIO as GPIO
import json
import time

GPIO.setmode(GPIO.BCM)

SENSORS = [4, 27, 22, 5, 6, 13, 26, 12, 25, 24]
DATA = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

for x in SENSORS:
    GPIO.setup(x, GPIO.IN)

while True:
    for x in SENSORS:
        DATA[SENSORS.index(x)] = GPIO.input(x)
    print(DATA)
    with open("/var/www/html/data.json", "w") as outfile:
        tosend = {
            "data": DATA
        }
        outfile.write(json.dumps(tosend))
    time.sleep(1)
